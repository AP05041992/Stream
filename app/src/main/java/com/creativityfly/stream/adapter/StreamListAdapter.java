package com.creativityfly.stream.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.creativityfly.stream.R;
import com.creativityfly.stream.model.StreamItem;

import java.util.ArrayList;


public class StreamListAdapter extends RecyclerView.Adapter<StreamListAdapter.MyViewHolder> {

    private ArrayList<StreamItem> streamItems;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView list_stream_imageView_up ;
        ImageView list_stream_imageView_down ;

        TextView list_stream_textView_name;
        TextView list_stream_textView_url ;
        TextView list_stream_textView_contact ;
        TextView list_stream_textView_up ;
        TextView list_stream_textView_down ;


        public MyViewHolder(View view)
        {
            super(view);

            list_stream_imageView_up = (ImageView) view.findViewById(R.id.list_stream_imageView_up);
            list_stream_imageView_down = (ImageView) view.findViewById(R.id.list_stream_imageView_down);

            list_stream_textView_name = (TextView) view.findViewById(R.id.list_stream_textView_name);
            list_stream_textView_url = (TextView) view.findViewById(R.id.list_stream_textView_url);
            list_stream_textView_contact = (TextView) view.findViewById(R.id.list_stream_textView_contact);
            list_stream_textView_up = (TextView) view.findViewById(R.id.list_stream_textView_up);
            list_stream_textView_down = (TextView) view.findViewById(R.id.list_stream_textView_down);
        }
    }


    public StreamListAdapter(Context context, ArrayList<StreamItem> streamItems) {
        this.streamItems = streamItems;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_stream, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {

        holder.list_stream_textView_name.setText(streamItems.get(position).getName());
        holder.list_stream_textView_url.setText(streamItems.get(position).getUrl());
        holder.list_stream_textView_contact.setText(streamItems.get(position).getContact());
        holder.list_stream_textView_up.setText(streamItems.get(position).getUpVote());
        holder.list_stream_textView_down.setText(streamItems.get(position).getDownVote());

        if (streamItems.get(position).isUpVoted())
        {
            holder.list_stream_imageView_up.setImageResource(R.drawable.up_filled);
        }
        else
        {
            holder.list_stream_imageView_up.setImageResource(R.drawable.up);
        }

        if (streamItems.get(position).isDownVoted())
        {
            holder.list_stream_imageView_down.setImageResource(R.drawable.down_filled);
        }
        else
        {
            holder.list_stream_imageView_down.setImageResource(R.drawable.down);
        }

    }

    @Override
    public int getItemCount() {
        return streamItems.size();
    }
}
