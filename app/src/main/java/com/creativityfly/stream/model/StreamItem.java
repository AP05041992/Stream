package com.creativityfly.stream.model;

/**
 * Created by android on 3/6/16.
 */
public class StreamItem
{
    String id;
    String name;
    String url;
    String contact;
    String upVote;
    String downVote;
    boolean isUpVoted;
    boolean isDownVoted;

    public StreamItem(String id, String name, String url, String contact, String upVote, String downVote, boolean isUpVoted, boolean isDownVoted) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.contact = contact;
        this.upVote = upVote;
        this.downVote = downVote;
        this.isUpVoted = isUpVoted;
        this.isDownVoted = isDownVoted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUpVote() {
        return upVote;
    }

    public void setUpVote(String upVote) {
        this.upVote = upVote;
    }

    public String getDownVote() {
        return downVote;
    }

    public void setDownVote(String downVote) {
        this.downVote = downVote;
    }

    public boolean isUpVoted() {
        return isUpVoted;
    }

    public void setUpVoted(boolean upVoted) {
        isUpVoted = upVoted;
    }

    public boolean isDownVoted() {
        return isDownVoted;
    }

    public void setDownVoted(boolean downVoted) {
        isDownVoted = downVoted;
    }
}
