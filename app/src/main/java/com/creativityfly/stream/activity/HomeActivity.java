package com.creativityfly.stream.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.creativityfly.stream.R;
import com.creativityfly.stream.adapter.StreamListAdapter;
import com.creativityfly.stream.model.StreamItem;

import java.util.ArrayList;

public class HomeActivity extends ParentActivity {

    private ArrayList<StreamItem> streamItems;
    private StreamListAdapter streamListAdapter;
    private RecyclerView activity_home_recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setStatusBar();
        setActionBarCustom(getString(R.string.app_name),false);

        findViewByIds();

    }

    public void findViewByIds()
    {
        activity_home_recycler_view = (RecyclerView)findViewById(R.id.activity_home_recycler_view);

        streamItems = new ArrayList<StreamItem>();
        streamItems.add(new StreamItem("1","Amit Padmani","http://www.amitpadmani.stream.com","+91 9924496190","120","10",true,false));
        streamItems.add(new StreamItem("1","Abhi K","http://www.abhi.stream.com","+91 8866112244","208","117",false,false));
        streamItems.add(new StreamItem("1","jems bond","http://www.abc.stream.com","+91 9924452190","12","0",false,false));
        streamItems.add(new StreamItem("1","Patrl Parth","http://www.youtube.stream.com","+91 8425696190","725","68",false,true));
        streamItems.add(new StreamItem("1","Amit Padmani","http://www.bbcc.stream.com","+91 9924496190","12","112",true,false));

        streamListAdapter = new StreamListAdapter(HomeActivity.this,streamItems);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        activity_home_recycler_view.setLayoutManager(linearLayoutManager);
        activity_home_recycler_view.setItemViewCacheSize(streamItems.size());
        activity_home_recycler_view.setAdapter(streamListAdapter);


    }
}
