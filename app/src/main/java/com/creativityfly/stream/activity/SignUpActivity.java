package com.creativityfly.stream.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creativityfly.stream.R;


public class SignUpActivity extends ParentActivity
{

    private LinearLayout activity_signup_linearLayout_signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        setStatusBar();
        findViewByIds();


        activity_signup_linearLayout_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
    }

    public void findViewByIds()
    {
        activity_signup_linearLayout_signin= (LinearLayout) findViewById(R.id.activity_signup_linearLayout_signin);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
