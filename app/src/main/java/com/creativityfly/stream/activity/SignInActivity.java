package com.creativityfly.stream.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creativityfly.stream.R;


public class SignInActivity extends ParentActivity
{

    private TextView activity_signin_textView_forgot_password;
    private Button activity_signin_button_login;
    private LinearLayout activity_signin_linearLayout_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        setStatusBar();
        findViewByIds();

        activity_signin_textView_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
//                forgotPasswordDialog();
            }
        });


        activity_signin_button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(intent);
            }
        });

        activity_signin_linearLayout_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),SignUpActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    public void findViewByIds()
    {
        activity_signin_textView_forgot_password = (TextView)findViewById(R.id.activity_signin_textView_forgot_password);
        activity_signin_button_login = (Button) findViewById(R.id.activity_signin_button_login);
        activity_signin_linearLayout_signup = (LinearLayout) findViewById(R.id.activity_signin_linearLayout_signup);

    }

    /*  Developer Name: Amit Padmani.
       Description: showing dialog(Forgot password) on screen.
       parameter : null
       Return type: null
    */

//    public void forgotPasswordDialog()
//    {
//        final Dialog dialogForgotPassword = new Dialog(SignInActivity.this);
//        dialogForgotPassword.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialogForgotPassword.setContentView(R.layout.dialog_forgot_password);
//        dialogForgotPassword.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        dialogForgotPassword.setCancelable(true);
//        dialogForgotPassword.show();
//
//        ImageView dialog_forgot_password_imageView_close = (ImageView) dialogForgotPassword.findViewById(R.id.dialog_forgot_password_imageView_close);
//
//        dialog_forgot_password_imageView_close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view)
//            {
//                dialogForgotPassword.dismiss();
//            }
//        });
//    }

}
