package com.creativityfly.stream.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import com.creativityfly.stream.R;

public class SplashActivity extends AppCompatActivity {


    private Handler myHandler;
    private int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        myHandler = new Handler();
        myHandler.postDelayed(myRunnable, SPLASH_TIME_OUT);
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {

            finish();
            Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
            startActivity(intent);

        }
    };

    protected void onStart() {
        super.onStart();
//        myHandler = new Handler();
//        myHandler.postDelayed(myRunnable, SPLASH_TIME_OUT);

    }

    protected void onStop() {
        super.onStop();
        myHandler.removeCallbacks(myRunnable);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (myHandler==null)
            {

            }
            else {
                myHandler.removeCallbacks(myRunnable);
            }
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
